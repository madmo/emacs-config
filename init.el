;; utf-8
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
;; This from a japanese individual.  I hope it works.
(setq default-buffer-file-coding-system 'utf-8)
;; From Emacs wiki
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;;no splash
(setq inhibit-startup-message t)

;; make backspace work again wtf!!
(global-set-key "\C-h" 'delete-backward-char)

;; no backup files
(setq make-backup-files nil)

;; case insentitive search
(setq case-fold-search t)

;; tab width of four
(setq-default tab-width 3)

;; C Indention settings
(setq c-default-style "k&r" c-basic-offset 3)

;; alternative load path
(add-to-list 'load-path (expand-file-name "~/.emacs.d"))
(package-initialize)

;; smart tabs!!!
(require 'smarttabs)
(smart-tabs-advice cperl-indent-line cperl-indent-level)

;;real smart tabs!!!
(require 'smart-tab)
(global-set-key (kbd "TAB")   'smart-tab)

(add-hook 'cperl-mode-hook
	  (lambda ()
	    (local-set-key (kbd "TAB")   'smart-tab)
	    ))

(setq smart-tab-using-hippie-expand nil)

;;show whitespaces
(setq-default show-trailing-whitespace t)

;;smooth scrolling
(require 'smooth-scrolling)

;;fuzzy searching
(require 'fuzzy)
(turn-on-fuzzy-isearch)

;; snippets
(yas-global-mode 1)

;; helm
(add-to-list 'load-path "~/.emacs.d/helm")
(require 'helm-config)
(global-set-key (kbd "C-c h") 'helm-mini)

;; speedbar
(require 'sr-speedbar)

;; simple projects
(add-to-list 'load-path "~/.emacs.d/eproject")
(require 'eproject)

;; tags hilight
(font-lock-add-keywords 'c-mode
 '(("\\<\\(FIXME\\|HACK\\|XXX\\|TODO\\)" 1 font-lock-warning-face prepend)))

;; clean interface
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

;; ############### Special language modes ###############

;; use the cool cperlmode
(defalias 'perl-mode 'cperl-mode)
(setq auto-mode-alist (append (list (cons "\\.pl\\'" 'perl-mode)) auto-mode-alist))
(setq auto-mode-alist (append (list (cons "\\.hooks\\'" 'perl-mode)) auto-mode-alist))
(add-to-list 'magic-mode-alist '("#!.*akrun" . perl-mode))

;;vala mode
(autoload 'vala-mode "vala-mode" "Major mode for editing Vala code." t)
(add-to-list 'auto-mode-alist '("\\.vala$" . vala-mode))
(add-to-list 'auto-mode-alist '("\\.vapi$" . vala-mode))
(add-to-list 'file-coding-system-alist '("\\.vala$" . utf-8))
(add-to-list 'file-coding-system-alist '("\\.vapi$" . utf-8))

;; go mode
(require 'go-mode-load)
(add-hook 'before-save-hook #'gofmt-before-save)

;; go completition
(require 'go-autocomplete)

;; coffescript mode
(require 'coffee-mode)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (wombat)))
 '(delete-selection-mode nil)
 '(mark-even-if-inactive t)
 '(scroll-bar-mode (quote right))
 '(show-paren-mode t)
 '(sr-speedbar-auto-refresh nil)
 '(sr-speedbar-right-side nil)
 '(sr-speedbar-skip-other-window-p t)
 '(transient-mark-mode 1))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "unknown" :slant normal :weight normal :height 90 :width normal)))))
